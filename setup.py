"""This module sets the package up."""

# Standard library imports
from setuptools import find_packages, setup


setup(
    name="dep_arrows",
    author="Michał",
    version="0.1",
    description="A quick example how to visualize action-to-object relation.",
    long_description=open("README.md").read(),
    package=find_packages(),
    python_requires=">= 3.9.0",
    install_requires=[
        "spacy",
        ("pl_core_news_sm @"
         "https://github.com/explosion/spacy-models/"
         "releases/download/pl_core_news_sm-3.0.0/"
         "pl_core_news_sm-3.0.0.tar.gz"),
    ],
    entry_points={
        "console_scripts": [
            "show-arrows=dep_arrows.console:main",
        ],
    },
)

