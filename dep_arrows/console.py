"""Entrypoint for the CLI command."""

# Standard library imports
import argparse

# Third-party library imports
import spacy
from spacy import displacy
from spacy.lang.pl.examples import sentences

def main() -> None:
    """Show a dependency parse tree or named entities for Polish sentences."""
    parser = argparse.ArgumentParser(description=main.__doc__)
    parser.add_argument(
        "--type",
        "-t",
        help="Specify visualization type",
        choices=["dep", "ent"],
        default="dep",
    )
    args = parser.parse_args()

    nlp = spacy.load("pl_core_news_sm")
    doc = nlp.pipe(sentences)

    options = {
        "compact": True,
        "color": "green"
    }
    displacy.serve(doc, style=args.type, options=options)


if __name__ == "__main__":
    main()
