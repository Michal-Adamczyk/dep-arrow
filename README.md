# Visualize Action-to-Object Dependency Relation

This example uses Spacy visualizer to show one way in which the dependency
relation between an action and a dependent object can be drawn. It uses arrows
rendered above the text to indicate, for example, an adjectival modifier of a
noun (mocna + kawa). I would suggest to implement a visually similar solution
that allows users to draw arrows going from an action to object in a graphic
interface. The general assumption is that one action can take multiple objects,
unlike the examples served.


## Installation

For simplicity, install the package in the editable mode from the package root.

```sh
python3 -m venv .
source bin/activate
python -m pip install -e .
show-arrows
```

## Comments

There is much to it. You can switch to named entities style by passing `--type
ent` to the CLI. Type `show-arrows --help` to show the package description and
available parameters.
